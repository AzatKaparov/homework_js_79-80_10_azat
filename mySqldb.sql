DROP DATABASE if exists homework_79;
CREATE DATABASE if not exists homework_79;

use homework_79;

CREATE TABLE if not exists categories (
    id int not null auto_increment primary key,
    category varchar(255) not null,
    description text null
);

CREATE TABLE if not exists places (
    id int not null auto_increment primary key,
    place varchar(255) not null,
    description text null
);

CREATE TABLE if not exists items (
    id int not null auto_increment primary key,
    category_id int not null,
    place_id int not null,
    name varchar(255) not null,
    description text null,
    image varchar(255) null,
    constraint item_category_id_fk
    foreign key (category_id)
    references categories(id)
    on update CASCADE
    on delete RESTRICT,
    constraint item_place_id_fk
    foreign key (place_id)
    references places(id)
    on update CASCADE
    on delete RESTRICT
);

INSERT INTO categories (category, description)
values ("Техника", "Всякого рода техническое оборудование"),
       ("Мебель", "Мебель в офисе"),
       ("Канцтовары", "Ручки, бумага и т.д.");

INSERT INTO places (place, description)
values ("Кабинет гл. директора", null),
       ("Первый этаж", null),
       ("Второй этаж", null),
       ("Кладовая", null),
       ("Склад", null);

INSERT INTO items (category_id, place_id, name, description, image)
values (1, 1, "Macbook pro 17", "Ноутбук глав.директора", "https://www.likenewpcs.com/wp-content/uploads/2016/09/MacBook-Pro-17-_2-scaled.jpg"),
       (2, 2, "Диван", "Диван для отдыха", "https://www.las.ru/media/catalog/categories/divan-trend-catalog-foto-x.jpg"),
       (3, 3, "Пачка бумаги А4", "Нет блин дерева 3Д", null);