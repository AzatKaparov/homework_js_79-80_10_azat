const express = require("express");
const mysql = require('../../mySqlConfig');

const router = express.Router();

router.get("/", async (req, res) => {
    const [places] = await mysql.getConnection().query(`SELECT id, place FROM ??`, ['places']);
    res.send(places);
});

router.get('/:id', async (req, res) => {
    const [place] = await mysql.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['places', req.params.id]);
    if (!place) {
        return res.status(404).send({error: 'Places not found'});
    }
    res.send(place[0]);
});


router.post("/", async (req, res) => {
    if (!req.body.place || req.body.place === "" || req.body.place === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const place = {
        place: req.body.place,
        description: req.body.description ? req.body.description : null
    };

    const newPlace = await mysql.getConnection().query(
        'INSERT INTO ?? (place, description) values (?, ?)',
        ['places', place.place, place.description]
    );

    res.send({
        ...place,
        id: newPlace.insertId
    });
});

router.put("/:id", async (req, res) => {
    if (!req.params.id || !req.body.place) {
        return res.status(400).send({error: "Invalid data or request"});
    }

    const place = {
        id: req.params.id,
        place: req.body.place,
        description: req.body.description
    };

    await mysql.getConnection().query(
        "UPDATE ?? SET ? where id = ?",
        ['places', {...place}, req.params.id]
    );

    const updatedPlace = await mysql.getConnection().query(
        "SELECT * FROM ?? where id = ?",
        ['places', req.params.id]
    );

    res.send(updatedPlace[0]);
});

router.delete("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }

    await mysql.getConnection().query(
        "DELETE FROM ?? where id = ?",
        ['places', req.params.id]
    ).catch(e => console.error(e));

    res.send({
        message: "Place deleted"
    });
});

module.exports = router;