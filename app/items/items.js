const express = require("express");
const mysql = require('../../mySqlConfig');
const multer = require('multer');
const config = require('../../config');
const {nanoid} = require('nanoid');
const path = require('path');



const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.get("/", async (req, res) => {
    const [items] = await mysql.getConnection().query(`SELECT id, name, category_id, place_id FROM ??`, ['items']);
    res.send(items);
});

router.get('/:id', async (req, res) => {
    const [item] = await mysql.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['items', req.params.id]);
    if (!item) {
        return res.status(404).send({error: 'Items not found'});
    }
    res.send(item[0]);
});


router.post("/", upload.single('image'), async (req, res) => {
    if (!req.body.name || req.body.name === "" || req.body.name === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.category_id || req.body.category_id === "" || req.body.category_id === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.place_id || req.body.place_id === "" || req.body.place_id === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const item = {
        name: req.body.name,
        category_id: req.body.category_id,
        place_id: req.body.place_id,
        description: req.body.description ? req.body.description : null
    };

    if (req.file) {
        item.image = req.file.filename;
    }

    const newItem = await mysql.getConnection().query(
        'INSERT INTO ?? (name, description, category_id, place_id) values (?, ?, ?, ?)',
        ['items', item.name, item.description, item.category_id, item.place_id]
    );

    res.send({
        ...item,
        id: newItem.insertId
    });
});

router.put("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }
    if (!req.body.name || req.body.name === "" || req.body.name === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.category_id || req.body.category_id === "" || req.body.category_id === null) {
        return res.status(400).send({error: 'Data not valid'});
    }
    if (!req.body.place_id || req.body.place_id === "" || req.body.place_id === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const item = {
        name: req.body.name,
        category_id: req.body.category_id,
        place_id: req.body.place_id,
        description: req.body.description ? req.body.description : null
    };

    if(req.file) item.image = req.file.filename;

    await mysql.getConnection().query(
        "UPDATE ?? SET ? where id = ?",
        ['items', {...item}, req.params.id]
    );

    const updatedItem = await mysql.getConnection().query(
        "SELECT * FROM ?? where id = ?",
        ['items', req.params.id]
    );

    res.send(updatedItem[0]);
});

router.delete("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }

    await mysql.getConnection().query(
        "DELETE FROM ?? where id = ?",
        ['items', req.params.id]
    ).catch(e => console.error(e));

    res.send({
        message: "Item deleted"
    });
});

module.exports = router;