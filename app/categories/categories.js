const express = require("express");
const mysql = require('../../mySqlConfig');

const router = express.Router();

router.get("/", async (req, res) => {
    const [categories] = await mysql.getConnection().query(`SELECT id, category FROM ??`, ['categories']);
    res.send(categories);
});

router.get('/:id', async (req, res) => {
    const [category] = await mysql.getConnection().query(
        `SELECT * FROM ?? where id = ?`,
        ['categories', req.params.id]);
    if (!category) {
        return res.status(404).send({error: 'Category not found'});
    }
    res.send(category[0]);
});


router.post("/", async (req, res) => {
    if (!req.body.category || req.body.category === "" || req.body.category === null) {
        return res.status(400).send({error: 'Data not valid'});
    }

    const category = {
        category: req.body.category,
        description: req.body.description ? req.body.description : null
    };

    const newCategory = await mysql.getConnection().query(
        'INSERT INTO ?? (category, description) values (?, ?)',
        ['categories', category.category, category.description]
    );

    res.send({
        ...category,
        id: newCategory.insertId
    });
});

router.put("/:id", async (req, res) => {
    if (!req.params.id || !req.body.category) {
        return res.status(400).send({error: "Invalid data or request"});
    }

    const category = {
        id: req.params.id,
        category: req.body.category,
        description: req.body.description
    };

    await mysql.getConnection().query(
        "UPDATE ?? SET ? where id = ?",
        ['categories', {...category}, req.params.id]
    );

    const updatedCategory = await mysql.getConnection().query(
        "SELECT * FROM ?? where id = ?",
        ['categories', req.params.id]
    );

    res.send(updatedCategory[0]);
});

router.delete("/:id", async (req, res) => {
    if (!req.params.id) {
        return res.status(400).send({error: "Invalid request"});
    }

    await mysql.getConnection().query(
        "DELETE FROM ?? where id = ?",
        ['categories', req.params.id]
    ).catch(e => console.error(e));

    res.send({
        message: "Category deleted"
    });
})

module.exports = router;