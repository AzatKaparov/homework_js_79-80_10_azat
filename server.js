const express = require('express');
const cors = require('cors');
const app = express();
const categories = require('./app/categories/categories');
const places = require('./app/places/places');
const items = require('./app/items/items');
const port = 8000;
const mysql = require('./mySqlConfig');


app.use(express.json());
app.use(cors());
app.use('/categories', categories);
app.use('/places', places);
app.use('/items', items);
app.use(express.static('public'));


mysql.connect().catch(e => console.error(e));

app.listen(port, () => {
    console.log(`Server started on ${port} port!`);
});